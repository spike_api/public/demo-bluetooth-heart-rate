# Heart Rate Sensor Demo

This web app demonstrates the use of the Web Bluetooth API for getting Heart
Rate from a nearby Bluetooth device supporting the standardized Heart Rate
service.
